from setuptools import setup
from setuptools import find_packages

with open("README.rst", "r") as fh:
    long_description = fh.read()

setup(
    name="easyDOI",
    version="0.0.1",
    description='N.A.',
    long_description=long_description,
    long_description_content_type="text/rst",
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    install_requires=['pyyaml', 'habanero', 'pybtex'],
    extras_require={},
    entry_points={
        'console_scripts': [
            'easyDOI=easy_doi.main:main'
        ],
    },
    author="",
    author_email="",
    url="",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
