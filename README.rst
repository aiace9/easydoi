easy DOI
========
placeholder

Install
=======

In the easyDOI folder

.. code-block:: bash

   pip install -e"."

Usage
=====
search:

.. code-block:: bash

   easyDOI -s "text to search through crossref API"

Configuration
=============

create a ``config`` file in yaml format in one of the following folders

- ``$HOME/.easyDOI/``
- ``$XDG_CONFIG_HOME/easyDOI/``

example:

.. code-block:: yaml

   ---
   generic:
     bib_file: "path/to/bibfile.bib""
