""" Load config file
 """
import os
import logging

from typing import Optional

import yaml

logger = logging.getLogger(__name__)

CONFIG_FOLDER = 'easyDOI'

DEFAULT = {
    'generic': {'pippo': None}
}


class ConfFileWrapper():
    """Wrapper for configuration files

    Parameters
    ----------
    file_name: file to load (full path)
    """

    def __init__(self, file_name: Optional[str] = None):
        home = os.environ['HOME'] + f'/.{CONFIG_FOLDER}/config'
        xdg = os.environ['XDG_CONFIG_HOME'] + f'/{CONFIG_FOLDER}/config'
        if file_name:
            self._config = file_name
            logger.debug('provided configuration file used')
        elif os.path.isfile(home):
            self._config = home
            logger.debug('home configuration file used')
        elif os.path.isfile(xdg):
            self._config = xdg
            logger.debug('xdg configuration file used')
        else:
            self._config = None
            self.yaml = DEFAULT
            return

        with open(self._config) as f:
            self.yaml = yaml.safe_load(f)

    @property
    def generic(self):
        return self.yaml['generic']
