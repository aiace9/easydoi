import pybtex.database as bib_db
from pybtex.scanner import PybtexSyntaxError

def parse_file(bib_file):
    dois = {}
    misc = []
    try:
        parsed = bib_db.parse_file(bib_file)
        for k, v in parsed.entries.items():
            doi = v.fields.get('doi', None)
            if doi:
                dois[doi] = v
            else:
                misc.append(v)
    except PybtexSyntaxError as e:
        print('=' * 8)
        print('Error during bib file reading:')
        print('\n', e, '\n')
        print('=' * 8 + '\n\n')
    return dois
