# https://github.com/alejandrogallo/python-scihub/blob/master/scihub/__init__.py
from habanero import Crossref
from requests.exceptions import HTTPError

def _create_author(author_info):
    if 'family' in author_info:
        return author_info['family']
    elif 'name' in author_info:
        return author_info['name']
    else:
        return f'dbg {author_info.keys()}'

def print_search_resutls(items, dois):
    for idx, item in enumerate(items):
        doi = item['DOI']
        s = f'===[ {idx} '
        if doi in dois.keys():
            s += '\u2714'
        s += ' ]==== \n'
        s += '::'.join(item.get('title', ['N.A.', ])) + '\n'
        s += f"DOI: {doi}\n"
        if 'author' in item:
            tmp = 'Author'
            tmp += 's: ' if len(item['author']) > 1 else ': '
            s+= f'{tmp}' + ', '.join([_create_author(author) for author in item['author']]) + '\n'
        print(s)


class CrossRefSearch():
    def __init__(self):
        self.cr = Crossref()

    def __call__(self, search, limit=5):
        try:
            query = self.cr.works(query=search, limit=5, sort='relevance')
        except HTTPError as err:
            if err.response.status_code == 504:
                print('SERVER ERROR, Gateway Timeout')
                exit(0)
        items = query['message']['items']
        return items


