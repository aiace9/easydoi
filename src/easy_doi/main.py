import argparse

from habanero import cn

from easy_doi.search import CrossRefSearch
from easy_doi.search import print_search_resutls
from easy_doi.configuration import ConfFileWrapper
from easy_doi.bibtex import parse_file


def main():
    parser = argparse.ArgumentParser(description='does stuff')
    command_group = parser.add_mutually_exclusive_group()
    command_group.add_argument('-s', '--search', type=str, help='in path',
                               )
    command_group.add_argument('-d', '--doi', type=str, help='in path',
                               )

    args = parser.parse_args()

    conf = ConfFileWrapper()
    dois = parse_file(conf.generic['bib_file'])

    if args.search:
        search = CrossRefSearch()
        items = search(args.search)
        print_search_resutls(items, dois)

        while True:
            sel = input('Select an option [a?][0]: ')
            sel = sel.strip()
            if len(sel) == 0:
                sel = 0
            elif sel.isdigit():
                pass
            elif sel[0].isalpha() and sel[1:].isdigit():
                print('command issued/not yet supported')
                sel = sel[1:]
            elif sel.isalpha() and sel == 'q':
                print('Exiting')
                exit()
            else:
                print('not a valid entry')
                continue

            sel = int(sel)

            if sel > (len(items) - 1):
                print(f'Not a valid value.')
                continue
            break
        doi = items[sel]['DOI']
    elif args.doi:
        doi = args.doi
    else:
        print('nothing passed')
        return
    content = cn.content_negotiation(ids=doi)
    print(content)


if __name__ == '__main__':
    main()
